########################################
# Guided NSX environment standup script.
# Nick Bradford
# Nbradford@vmware.com
#

#Copyright © 2015 VMware, Inc. All Rights Reserved.

#Permission is hereby granted, free of charge, to any person obtaining a copy of
#this software and associated documentation files (the "Software"), to deal in 
#the Software without restriction, including without limitation the rights to 
#use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
#of the Software, and to permit persons to whom the Software is furnished to do 
#so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all 
#copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
#SOFTWARE.

#Requires -version 3.0
#Requires -modules PowerNSX, VMware.VimAutomation.Core

#############################################
#############################################
$Version = "0.1"

# NSX Infrastructure Configuration.  Adjust to suit environment.

#Default Values:
$Default_vCenterUserName = "administrator@vsphere.local"
$Default_TransportZoneName = "TransportZone1"
$Default_SegmentPoolStart = "5000"
$Default_SegmentPoolEnd = "5999"
$Default_VxlanMtu = 1600
$Default_SyslogPort = 514
$Default_SyslogProtocol = "TCP"
$Default_NsxManagerName = "NsxManager"

$domain_regex = "^((([a-zA-Z]{1})|([a-zA-Z]{1}[a-zA-Z]{1})|([a-zA-Z]{1}[0-9]{1})|([0-9]{1}[a-zA-Z]{1})|([a-zA-Z0-9][a-zA-Z0-9-_]{1,61}[a-zA-Z0-9]))\.)+(([a-zA-Z]{1})|([a-zA-Z]{1}[a-zA-Z]{1})|([a-zA-Z]{1}[0-9]{1})|([0-9]{1}[a-zA-Z]{1})|([a-zA-Z0-9][a-zA-Z0-9-_]{1,61}[a-zA-Z0-9]))$"

$SysLogPort = $Default_SyslogPort
$SysLogProtocol = $Default_SyslogProtocol
$VxlanMtuSize = $Default_VxlanMtu


# #############################################
# #############################################
# # Logical Topology environment 

# $EdgeUplinkPrimaryAddress = "192.168.100.192"
# $EdgeUplinkSecondaryAddress = "192.168.100.193"
# $EdgeUplinkNetworkName = "Internal"
# $AppliancePassword = "VMware1!VMware1!"
# $BooksvAppLocation = "<pathToBooksVapp>"


# ############################################
# ############################################
# # Topology Details.  No need to modify below here 

# #Names
# $TsTransitLsName = "Transit"
# $TsWebLsName = "Web"
# $TsAppLsName = "App"
# $TsDbLsName = "Db"
# $TsMgmtLsName = "Mgmt"
# $TsEdgeName = "Edge01"
# $TsLdrName = "Dlr01"

# #Topology
# $EdgeInternalPrimaryAddress = "172.16.1.1"
# $EdgeInternalSecondaryAddress = "172.16.1.6"
# $LdrUplinkPrimaryAddress = "172.16.1.2"
# $LdrUplinkProtocolAddress = "172.16.1.3"
# $LdrWebPrimaryAddress = "10.0.1.1"
# $WebNetwork = "10.0.1.0/24"
# $LdrAppPrimaryAddress = "10.0.2.1"
# $AppNetwork = "10.0.2.0/24"
# $LdrDbPrimaryAddress = "10.0.3.1"
# $DbNetwork = "10.0.3.0/24"
# $TransitOspfAreaId = "10"
# $DefaultSubnetMask = "255.255.255.0"
# $DefaultSubnetBits = "24"

# #3Tier App
# $vAppName = "Books"

# #WebTier VMs
# $Web01Name = "Web01"
# $Web01Ip = "10.0.1.11"
# $Web02Name = "Web02"
# $Web02Ip = "10.0.1.12"

# #AppTier VMs
# $App01Name = "App01"
# $App01Ip = "10.0.2.11"
# $App02Name = "App02"
# $App02Ip = "10.0.2.12"
# $Db01Name = "Db01"
# $Db01Ip = "10.0.3.11"

# #DB Tier VMs
# $Db02Name = "Db02"
# $Db02Ip = "10.0.3.12"

# ##LoadBalancer
# $LbAlgo = "round-robin"
# $WebpoolName = "WebPool1"
# $ApppoolName = "AppPool1"
# $WebVipName = "WebVIP"
# $AppVipName = "AppVIP"
# $WebAppProfileName = "WebAppProfile"
# $AppAppProfileName = "AppAppProfile"
# $VipProtocol = "http"
# $HttpPort = "80"

# ## Security Groups
# $WebSgName = "SGWeb"
# $WebSgDescription = "Web Security Group"
# $AppSgName = "SGApp"
# $AppSgDescription = "App Security Group"
# $DbSgName = "SGDb"
# $DbSgDescription = "DB Security Group"
# $BooksSgName = "SGBooks"
# $BooksSgDescription = "Books ALL Security Group"

# #DFW
# $FirewallSectionName = "Bookstore"
# $LBMonitorName = "default_http_monitor"

###############################################
# Do Not modify below here.
###############################################

###############################################
###############################################
# Constants

$WaitStep = 30
$WaitTimeout = 600
$yesnochoices = New-Object Collections.ObjectModel.Collection[Management.Automation.Host.ChoiceDescription]
$yesnochoices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&Yes'))
$yesnochoices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&No'))


function Invoke-Installation { 

    $PreppedViClusters = New-Object System.Collections.Arraylist
    $PreppedViVds = New-Object System.Collections.Arraylist
    
    #We are using the default connection here because the code Id written ages ago assumed it.  There are a couple of limitations
    #preventing me using a named connection at the moment.  If this annoys you, let me know.
    write-progress -Activity "Validating Configuration" -status "Connecting to vCenter"
    if ( $defaultViServer.IsConnected ) { 
        Disconnect-ViServer -confirm:$false
    }
    connect-ViServer $vCenterServer -Credential $vCenterCredentials -errorAction Stop -WarningAction Ignore | out-null
    
    
    write-progress -Activity "Validating Configuration" -status "Getting vCenter Objects"
  
    try { 

        $MgmtCluster = Get-Cluster $MgmtClusterName -errorAction Stop
        $MgmtDatastore = Get-Datastore $ManagementDatastoreName -errorAction Stop
        $ManagementPortGroup = Get-VdPortGroup $MgmtPgName -errorAction Stop
        foreach ( $cl_moref in $clusters.Keys ) { 
            $PreppedViClusters.Add(( Get-Cluster -id $cl_moref -errorAction Stop )) | out-null
        }
        foreach ( $vds_moref in $NsxVds.Keys ) { 
            $PreppedViVds.Add(( Get-VdSwitch -id $vds_moref -errorAction Stop )) | out-null

        }
    }
    catch { 
        Throw "Failed validating vSphere Environment. $_"
    }


    # 
    ###############################
    # Deploy NSX Manager appliance.

    write-progress -Activity "Deploying" -status "Deploying NSX Manager"
    [string]$ManagementNetworkSubnetMask = convertfrom-bitmask -BitMask $Ippools.$ManagementIpPool.Prefix

    try { 
        if ( $12GbManager ) { 
            $NsxManagerMem = 12
        }
        else { 
            $NsxManagerMem = 16
        }

        New-NsxManager -NsxManagerOVF $NsxManagerOVF -Name $NsxManagerName -ClusterName $MgmtClusterName -ManagementPortGroupName $MgmtPgName -DatastoreName $ManagementDatastoreName -CliPassword $NsxManagerPassword -CliEnablePassword $NsxManagerPassword -Hostname $NsxManagerName -IpAddress $NsxManagerIpAddress -Netmask $ManagementNetworkSubnetMask -Gateway $Ippools.$ManagementIpPool.Gateway -DnsServer $Ippools.$ManagementIpPool.PrimaryDNS -DnsDomain $Ippools.$ManagementIpPool.DomainName -NtpServer $NtpServer -EnableSsh -StartVM -Wait -FolderName vm -ManagerMemoryGB $NsxManagerMem | out-null

        Connect-NsxServer -server $NsxManagerIpAddress -Username 'admin' -password $NsxManagerPassword -DisableViAutoConnect -ViWarningAction Ignore | out-null
          
    }
    catch {
        throw "An error occured during NSX Manager deployment.  $_"
    }

    ###############################
    # Configure NSX Manager appliance.

    try { 
        write-progress -Activity "Deploying" -status "Configuring NSX Manager" 

        if ( $SyslogServer ) { 
            write-progress -Activity "Deploying" -status "Configuring NSX Manager" -currentoperation "Configuring Syslog Server"
            Set-NsxManager -SyslogServer $SyslogServer -SyslogPort $SysLogPort -SyslogProtocol $SysLogProtocol | out-null
        }

        write-progress -Activity "Deploying" -status "Configuring NSX Manager" -currentoperation "Configuring SSO"
        Set-NsxManager -SsoServer $VcenterServer -SsoUserName $vCenterUserName -SsoPassword $vCenterPassword | out-null

        write-progress -Activity "Deploying" -status "Configuring NSX Manager" -currentoperation "Registering NSX Manager with vCenter"
        Set-NsxManager -vCenterServer $VcenterServer -vCenterUserName $vCenterUserName -vCenterPassword $vCenterPassword | out-null

        write-progress -Activity "Deploying" -status "Configuring NSX Manager" -currentoperation "Establishing full PowerNSX session with NSX Manager"
        #Update the connection with VI connection details...
        Connect-NsxServer -server $NsxManagerIpAddress -Username 'admin' -password $NsxManagerPassword -VIUsername $vCenterUserName -VIPassword $vCenterPassword -ViWarningAction Ignore -DebugLogging | out-null

    }
    catch {

        throw "Exception occured configuring NSX Manager.  $_"
        
    }


    ###############################
    # Creating IP Pools

    foreach ( $ippool in $ippools.Keys ) {
        write-progress -Activity "Deploying" -status "Creating NSX IP Pools" -currentoperation "Creating IP Pool $ippool"
     
        New-NsxIpPool -Name $IpPool -Gateway $ippools.$ippool.Gateway -SubnetPrefixLength $ippools.$ippool.Prefix -DnsServer1 $ippools.$ippool.PrimaryDNS -DnsServer2 $ippools.$ippool.SecondaryDNS -DnsSuffix $ippools.$ippool.DomainName -StartAddress $ippools.$ippool.StartIp -EndAddress $ippools.$ippool.EndIp | out-null
    }


    ###############################
    # Deploy NSX Controllers

    write-progress -Activity "Deploying" -status "Configuring NSX Control Cluster"

    try {

        write-progress -Activity "Deploying" -status "Configuring NSX Control Cluster" -currentoperation "Getting Management IP Pool"
     
        $ManagementPool = Get-NsxIpPool $ManagementIpPool
        if ( $SingleControllerCluster ) { 
            $numcontrollers = 0
        }
        else {
            $numcontrollers = 2
        }

        for ( $i=0; $i -le $numcontrollers; $i++ ) { 

            write-progress -Activity "Deploying" -status "Configuring NSX Control Cluster" -currentoperation "Deploying NSX Controller $($i+1)"

            try {

                $Controller = New-NsxController -ipPool $ManagementPool -Cluster $MgmtCluster -datastore $MgmtDatastore -PortGroup $ManagementPortGroup -password $ControllerPassword -confirm:$false -wait
            }
            catch {
                throw "Controller $($i+1) deployment failed. $_"
            }
        }
    }
    catch {

        Throw  "Failed deploying controller Cluster.  $_"
    }



    ##############################
    # Prep VDS

    write-progress -Activity "Deploying" -status "Configuring Distributed Switches"

    try {
        #Currently only supporting LOADBALANCE_SRCID - yeah - ill get there...
        foreach ( $vds in $PreppedViVds) { 
            New-NsxVdsContext -VirtualDistributedSwitch $vds -Teaming "LOADBALANCE_SRCID" -Mtu $VxlanMtuSize | out-null
        }

    }
    catch {
        Throw  "Failed configuring VDS.  $_"

    }


    ##############################
    # Prep Clusters


    write-progress -Activity "Deploying" -status "Preparing Clusters"

    try {

        foreach ( $cluster in $clusters.Keys ) { 
            
            write-progress -Activity "Deploying" -status "Preparing Clusters" -currentoperation "Preparing Cluster $($clusters.$cluster.ClusterName)"

            if ( $clusters.$cluster.ClusterVxlanDhcp ) { 
                Get-Cluster -id $cluster | New-NsxClusterVxlanConfig -VirtualDistributedSwitch (Get-VdSwitch -id $clusters.$cluster.ClusterVds ) -Vlan ( $nsxVds.($clusters.$cluster.ClusterVds).VxlanVlanId) -VtepCount ( $nsxVds.($clusters.$cluster.ClusterVds).NumUplinks) | out-null
            }
            else { 
                Get-Cluster -id $cluster | New-NsxClusterVxlanConfig -VirtualDistributedSwitch (Get-VdSwitch -id $clusters.$cluster.ClusterVds ) -Vlan ( $nsxVds.($clusters.$cluster.ClusterVds).VxlanVlanId) -VtepCount ( $nsxVds.($clusters.$cluster.ClusterVds).NumUplinks) -ipPool (Get-NsxIpPool $clusters.$cluster.ClusterVxlanIpPool) | out-null
            }
        }
    }
    catch {
        Throw  "Failed preparing clusters for NSX.  $_"

    }

    ##############################
    # Configure Segment Pool

    write-progress -Activity "Deploying" -status "Configuring Segment Id Pool" 

    try {
        New-NsxSegmentIdRange -Name "SegmentIDPool" -Begin $SegmentPoolStart -end $SegmentPoolEnd | out-null
    }
    catch {
        Throw  "Failed configuring SegmentId Pool.  $_"
    }


    ##############################
    # Create Transport Zone
     
    write-progress -Activity "Deploying" -status "Configuring TransportZone" 

    try {
        #Configure TZ and add clusters.  We support UNICAST and.... UNICAST
        New-NsxTransportZone -Name $TransportZoneName -Cluster $PreppedViClusters -ControlPlaneMode "UNICAST_MODE" | out-null

    }
    catch {
        Throw  "Failed configuring Transport Zone.  $_"

    }
    write-progress -Activity "Deploying" -status "Configuring TransportZone"  -Completed

    $script:InstallationComplete = $true
    
}


###############################################
# Network Validation functions

function convertfrom-bitmask { 

    param (
        [Parameter(Mandatory=$true)]
            [ValidateRange(1,32)]
            [int]$Bitmask
    )

    [ipaddress]$base = "255.255.255.255"
    $invertedmask = [ipaddress]($base.address - [convert]::toint64(([math]::pow(2,(32-$bitmask)) -bxor $base.Address) + 1))
    [ipaddress]$subnetmask = "$(255-$($invertedmask.GetAddressBytes()[3]))." +
        "$(255-$($invertedmask.GetAddressBytes()[2]))." + 
        "$(255-$($invertedmask.GetAddressBytes()[1]))." + 
        "$(255-$($invertedmask.GetAddressBytes()[0]))"

    $subnetmask
}

function convertto-bitmask { 

    param (
        [Parameter(Mandatory=$true)]
            [ipaddress]$subnetmask
    )


    $bitcount = 0
    $boundaryoctetfound = $false
    $boundarybitfound = $false
    #start at most sig end.
    foreach ($octet in $subnetmask.GetAddressBytes()) {

        switch ($octet) { 
            "255" { 
                if ( $boundaryoctetfound ) { 
                    throw "SubnetMask specified is not valid.  Specify a valid mask and try again."
                } else {
                    $bitcount += 8
                }
            }

            "0" { $boundaryoctetfound = $true }

            default {
                if ( $boundaryoctetfound ) { 
                    throw "SubnetMask specified is not valid.  Specify a valid mask and try again."
                }
                else {
                    $boundaryoctetfound = $true
                    $boundaryoctet = $_

                    for ( $i = 7; $i -ge 0 ; $i-- ) { 
                        if ( $boundaryoctet -band [math]::pow(2,$i) ) { 
                            if ( $boundarybitfound) { 
                                #Already hit boundary - mask isnt valid.
                                throw "SubnetMask specified is not valid.  Specify a valid mask and try again."
                            }
                            $bitcount++
                        }
                        else { 
                            $boundarybitfound = $true
                        }
                    }
                }
            }
        }
    }

    $bitcount
}

function Get-NetworkFromHostAddress {
        
    [CmdletBinding(DefaultParameterSetName="mask")]

    param (
        [Parameter(Mandatory=$true,ParameterSetName="cidr")]
        [Parameter(Mandatory=$true,ParameterSetName="mask")]
            [ipaddress]$Address,
        [Parameter(Mandatory=$true,ParameterSetName="mask")]
            [ipaddress]$SubnetMask,
        [Parameter(Mandatory=$true,ParameterSetName="cidr")]
            [ValidateRange(1,32)]
            [int]$BitMask
            
    )

    if ( $PsCmdlet.ParameterSetName -eq 'cidr') { 
        $SubnetMask = convertfrom-bitmask -bitmask $BitMask
    }

    $NetAddress = ""
    for ( $i = 0; $i -le 3; $i++ ) {
        
        $NetAddress += "$($Address.GetAddressBytes()[$i] -band $SubnetMask.GetAddressBytes()[$i])."
    }
    [ipaddress]($NetAddress -replace "\.$","")
}

function Test-AddressInNetwork { 

    [CmdletBinding(DefaultParameterSetName="mask")]
    param (
        [Parameter(Mandatory=$true,ParameterSetName="mask")]
            [ipaddress]$SubnetMask,
        [Parameter(Mandatory=$true,ParameterSetName="cidr")]
            [ValidateRange(1,32)]
            [int]$Bitmask,
        [Parameter(Mandatory=$true)]
            [ipaddress]$Network,
        [Parameter(Mandatory=$true)]
            [ipaddress]$Address
    )

    if ( $PsCmdlet.ParameterSetName -eq 'cidr') { 
        $SubnetMask = convertfrom-bitmask -bitmask $BitMask
    }
    $Network -eq (Get-NetworkFromHostAddress -Address $Address -SubnetMask $SubnetMask)
}

function Get-NetworkRange { 
    
    #Im well aware that this is very inefficient, but I need it quickly, and CPUs are cheap ;)

    [CmdletBinding(DefaultParameterSetName="mask")]
    param (
        [Parameter(Mandatory=$true,ParameterSetName="mask")]
            [ipaddress]$SubnetMask,
        [Parameter(Mandatory=$true,ParameterSetName="cidr")]
            [ValidateRange(1,32)]
            [int]$Bitmask,
        [Parameter(Mandatory=$true)]
            [ipaddress]$Network
    )

    if ( $PsCmdlet.ParameterSetName -eq 'cidr') { 
        $SubnetMask = convertfrom-bitmask -bitmask $BitMask
    }

    #Check that the network specified is a valid network address
    if ( -not (( Get-NetworkFromHostAddress -address $network -subnetmask $subnetmask ) -eq $network  )) { 
        throw "Specified Network address is not valid (Does not lie on subnet boundary)"
    }

    $Range = New-Object System.Collections.Arraylist
    $CurrAddressBytes = @( $Network.GetAddressBytes()[0], $Network.GetAddressBytes()[1], $Network.GetAddressBytes()[2], $Network.GetAddressBytes()[3])

    do { 


        $CurrAddressBytes[3] += 1
        if ( $CurrAddressBytes[3] -eq 256 ) {
            $CurrAddressBytes[3] = 0
            $CurrAddressBytes[2] += 1

            if ( $CurrAddressBytes[2] -eq 256 ) { 
                $CurrAddressBytes[2] = 0
                $CurrAddressBytes[1] + 1

                if ( $CurrAddressBytes[1] -eq 256 ) { 
                    $CurrAddressBytes[1] = 0
                    $CurrAddressBytes[0] + 1

                    if ( $CurrAddressBytes[0] -eq 256 ) { 
                        break
                    }
                }
            }
        }

        $currentaddress = "$($CurrAddressBytes[0]).$($CurrAddressBytes[1]).$($CurrAddressBytes[2]).$($CurrAddressBytes[3])"
        $Range.Add($currentaddress) | out-null

    } while ( Test-AddressInNetwork -network $network -subnetmask $subnetmask -address $currentaddress )
    
    #remove last and second last (last is above range, second last is broadcast address...  )
    $range.RemoveAt($range.Count - 1 )
    $BroadCastAddress = $range[-1]
    $range.RemoveAt($range.Count - 1 )

    [pscustomobject]@{ 
        "NetworkAddress" = $network
        "ValidAddressRange" = $range
        "Broadcast" = $BroadCastAddress
    }

}

function read-hostwithdefault { 

    param(

        [string]$Default,
        [string]$Prompt
    )
    
    if ($default) {
        $response = read-host -prompt "$Prompt [$Default]"
        if ( $response -eq "" ) {
            $Default
        }
        else {
            $response
        }
    }
    else { 
        read-host -prompt $Prompt
    }
}




################################################
# Menu Item functions

function Get-vCenterConnectionMenu { 
 
    #vSphere Config Menu Item 0
    $script:vCenterServer = read-hostwithdefault -default $vCenterServer -prompt "Enter vCenter Server Address"
    if ( $vCenterUserName ) { 
        $script:vCenterUserName = read-hostwithdefault -default $vCenterUserName "UserName" -prompt "Enter vCenter Credentials (Used for NSX Manager Registration and SSO configuration)"
    }
    else {
        $script:vCenterUserName = read-hostwithdefault -default $Default_vCenterUserName "UserName" -prompt "Enter vCenter Credentials (Used for NSX Manager Registration and SSO configuration)"
    }
    $vCenterPassword = Read-host "Password" -assecurestring
    $script:vCenterCredentials = New-Object System.Management.Automation.PSCredential ($vCenterUserName, $vCenterPassword)
    try { 
        $script:vCenterConnection = connect-ViServer $vCenterServer -Credential $vCenterCredentials -errorAction Stop -NotDefault -WarningAction Ignore
        $script:vCenterPassword = (New-Object System.Management.Automation.PSCredential 'N/A', $vCenterPassword).GetNetworkCredential().Password

    } 
    catch { 
        "Connection to $vCenterServer failed. ($_)"
    }
}

function Select-MgmtClusterMenu { 

    #vSphere Config Menu Item 1
    $MgmtClustersMenu= @{ 
        "Script" = {};
        "Enabled" = $false;
        "Name" = "Select Management Cluster";
        "HelpText" = "Valid vCenter connection required"
        "Items" = New-Object System.Collections.Arraylist
    }

    foreach ( $cluster in Get-Cluster -server $vCenterConnection ) {
        $MgmtClustersMenu.Items.Add( 
            @{ 
                "Name" = $cluster.name; "Enabled" = $true; "Selectable"=$true; "Selected"=$false; "Script" = { 
                    $script:MgmtClusterName = $MgmtClustersMenu.Items[$SelectedItemNumber].name 
                    #Enable filtered Datastore and vDS selection when Management cluster is known
                    set-variable -scope 1 -name menuexit -value $true
                }
            }
        ) | out-null
    }
    show-menu -menu $MgmtClustersMenu | out-null
}

function Select-MgmtDatastoreMenu { 
    
    #vSphere Config Menu Item 2
    $MgmtDatastoresMenu = @{ 
        "Script" = {};
        "Enabled" = $false;
        "Name" = "Select Management Datastore";
        "HelpText" = "Valid vCenter connection required"
        "Items" = New-Object System.Collections.Arraylist
    }

    foreach ( $ds in (Get-Cluster $MgmtClusterName -server $vCenterConnection | Get-Datastore ) ) {
        $MgmtDatastoresMenu.Items.Add(
            @{ 
                "Name" = $ds.name; "Enabled" = $true; "Selectable"=$true; "Selected"=$false; "Script" = { 
                    $script:ManagementDatastoreName = $MgmtDatastoresMenu.Items[$SelectedItemNumber].name 

                    #Exit the menu
                    set-variable -scope 1 -name menuexit -value $true
                }
            } 
        ) | out-null
    }
    show-menu -menu $MgmtDatastoresMenu | out-null
}

function Select-MgmtPgMenu { 
    
    #vSphere Config Menu Item 3
    $MgmtPgMenu = @{ 
        "Script" = {};
        "Enabled" = $false;
        "Name" = "Select Management Port Group";
        "HelpText" = "Valid vCenter connection required"
        "Items" = New-Object System.Collections.Arraylist
    }

    foreach ( $pg in (Get-Cluster $MgmtClusterName -server $vCenterConnection | Get-VMHost | Get-VDSwitch | Get-VDPortgroup | ? { $_.IsUplink -ne $true }) ) {
        $MgmtPgMenu.Items.Add(
            @{ 
                "Name" = $pg.name; "Enabled" = $true; "Selectable"=$true; "Selected"=$false; "Script" = { 
                    $script:MgmtPgName = $MgmtPgMenu.Items[$SelectedItemNumber].name 
                    set-variable -scope 1 -name menuexit -value $true

                }
            } 
        ) | out-null
    }
    show-menu -menu $MgmtPgMenu | out-null
}

Function Get-NsxManagerOVA { 
    
    $File = gci -filter *.ova
    if (  $File.count -eq 1 ) { 
        $DefaultOva = $File.name
        $FileName = read-hostwithdefault -default $DefaultOva -Prompt "Enter path to NSX Manager OVA"
    }
    else { 
        $FileName = read-host -Prompt "Enter path to NSX Manager OVA"
    }
    if ( test-path $FileName ) { 
        
        $Script:NsxManagerOVF = $FileName
    }
    else {
        "File Not Found"
    }
}

function Select-NsxMgmtIpPoolMenu { 

    $script:MgmtIpPoolMenu = @{ 
        "Script" = {};
        "Enabled" = $true;
        "Name" = "Select Ip Pool for Management Network addressing";
        "HelpText" = "Create or select IP Pool for Management Network addressing.  Valid vCenter configuration required"
        "Items" = New-Object System.Collections.Arraylist
    }

    $script:MgmtIpPoolMenu.Items.Add(
        @{ 
            "Name" = "Create New Ip Pool";
            "Enabled" = $true
            "Selectable" = $true
            "Selected" = $false
            "Script" = { 
                $NewPool = New-IpPool

                #We have to add the newly created pool to the existing menu...
                $script:MgmtIpPoolMenu.Items.Add(
                    @{ 
                        "Name" = $NewPool
                        "Enabled" = $true
                        "Selectable" = $true
                        "Selected" = ($ManagementIpPool -eq $newpool)
                        "ValidData" = ($ManagementIpPool -eq $newpool)
                        "Script" = { 
                            
                            write-host "Validating connectivity to specified pool gateway ($($ippools.$($MgmtIpPoolMenu.Items[$SelectedItemNumber].Name).Gateway))"
                            if ( -not (test-connection -quiet -count 2 $ippools.$($MgmtIpPoolMenu.Items[$SelectedItemNumber].Name).Gateway )) {
                                $message  = "Unable to ping the gateway address of the selected IP Pool.  This may indicate a problem that will cause deployment to fail"
                                $question = "Still use this ip pool?"
                                $decision = $Host.UI.PromptForChoice($message, $question, $yesnochoices, 1)
                            }    
                            else { $decision = 0 } 
                            if ($decision -eq 0) {
                                $script:ManagementIpPool = $MgmtIpPoolMenu.Items[$SelectedItemNumber].Name
                                set-variable -scope 1 -name menuexit -value $true 
                            }
                        }
                    } 
                ) | out-null
            }
        }
    ) | out-null

    foreach ( $ippool in $IpPools.Keys ) {
        $script:MgmtIpPoolMenu.Items.Add(
            @{ 
                "Name" = $IpPool
                "Enabled" = $true
                "Selectable" = $true
                "Selected" = ($ManagementIpPool -eq $ippool)
                "ValidData" = ($ManagementIpPool -eq $ippool)
                "Script" = {    
                    write-host "Validating connectivity to specified pool gateway ($($ippools.$($MgmtIpPoolMenu.Items[$SelectedItemNumber].Name).Gateway))"
                    if ( -not (test-connection -quiet -count 2 $ippools.$($MgmtIpPoolMenu.Items[$SelectedItemNumber].Name).Gateway )) {
                        $message  = "Unable to ping the gateway address of the selected IP Pool.  This may indicate a problem that will cause deployment to fail"
                        $question = "Still use this ip pool?"
                        $decision = $Host.UI.PromptForChoice($message, $question, $yesnochoices, 1)
                    }    
                    else { $decision = 0 } 
                    if ($decision -eq 0) {
                        $script:ManagementIpPool = $MgmtIpPoolMenu.Items[$SelectedItemNumber].Name
                        set-variable -scope 1 -name menuexit -value $true 
                    }
                }
            } 
        ) | out-null
    }
    
    show-menu -menu $MgmtIpPoolMenu | out-null
}


function Get-ManagementNetworkMenu { 
    
    $script:ManagementNetworkMenu = @{ 
        "Script" = {}
        "Enabled" = $false
        "Name" = "Configure Management Network Addressing"
        "HelpText" = "Configure IP Pool details for Controller addressing.  Valid vCenter connection required"
        "Items" = @(
            @{ 
                "Name" = "Configure Management Network IP Pool"
                "HelpText" = "Defines the IP allocation for NSX Controllers and the NSX Manager appliance"
                "Enabled" = $true
                "ValidData" = $ManagementIpPool
                "CompletedItemText" = $ManagementIpPool
                "Script" = {      

                    Select-NsxMgmtIpPoolMenu
                    update-status}
            },
            @{ 
                "Name" = "Specify NSX Manager IP"
                "ValidData" = $NsxManagerIpAddress
                "Enabled" = $ManagementIpPool
                "CompletedItemText" = $NsxManagerIpAddress
                "Script" = { 
                    #Get the mgr ip
                    [ipaddress]$TestIp = $null
                    do { 
                        $response = read-host -prompt "Enter an IP address in the subnet $($IpPools.$ManagementIpPool.NetworkAddress)/$($IpPools.$ManagementIpPool.Prefix)"
                    } while ( (-not ([ipaddress]::TryParse( $response, [ref]$TestIp) )) -or ( -not (Test-AddressInNetwork -network $IpPools.$ManagementIpPool.NetworkAddress -BitMask $IpPools.$ManagementIpPool.Prefix -address $response)))

                    $script:NsxManagerIpAddress = $response.ToString()
                    update-status
                }      
            }
        )
    }

    show-menu -menu $ManagementNetworkMenu | out-null
}


function Select-NsxVtepVdsMenu { 
    
    $script:NsxVtepVdsMenu = @{ 
        "Script" = {};
        "Enabled" = $false;
        "Name" = "Select NSX Virtual Distributed Switch";
        "HelpText" = "Valid vCenter connection required"
        "Items" = New-Object System.Collections.Arraylist
    }

    foreach ( $vds in (Get-Cluster $SelectedClusterName -server $vCenterConnection | Get-VMHost | Get-VDSwitch ) ) {
        $script:NsxVtepVdsMenu.Items.Add(
            @{ 
                "Name" = $vds.name
                "Enabled" = $true 
                "MoRef" = $vds.id
                "Selectable"=$True
                "Selected" = $Clusters.$SelectedClusterMoRef.ClusterVds -eq $vds.id
                "ValidData" = $Clusters.$SelectedClusterMoRef.ClusterVds -eq $vds.id
                "Script" = { 
                    $SelectedVdsMoRef = $NsxVtepVdsMenu.Items[$SelectedItemNumber].MoRef

                    if ( -not $NsxVds.Contains($SelectedVdsMoRef)) {
                        #VDS has not been selected for config already.  VLAN ID must be consistent for all hosts using the VDS, so user only gets one opportunity to set it.
                        $SelectedVdsName = $NsxVtepVdsMenu.Items[$SelectedItemNumber].Name
                        
                        try {

                            Do { $response = read-hostwithdefault -default 0 -prompt "Enter VTEP VLAN ID for selected VDS" } 
                            while ((0..4094) -notcontains $response)
                            $VxlanVlanID = $response

                            #Add the selected VDS to the global VDS dict.
                            $script:NsxVds.Add($SelectedVdsMoRef,
                                @{
                                    "VdsName" = $SelectedVdsName
                                    "VxlanVlanID" = $VxlanVlanID
                                    "NumUplinks" = (Get-VdSwitch -id $SelectedVdsMoRef -server $vCenterConnection | Get-VDPortgroup  | ? { $_.IsUplink }).numports 
                                }
                            ) | out-null
                        } catch {
                            "Invalid VLAN Id Specified.  $_"
                        }

                    }

                    #If the VDS has been added, the VLAN is valid, so we exit the submenu.
                    if ( $NsxVds.$SelectedVdsMoRef ) { 
                        $script:Clusters.$SelectedClusterMoRef.ClusterVds = $SelectedVdsMoRef
                        #Enable Addressing menu
                        $script:NsxClusterConfigMenu.Items[2].Enabled=$true
                        set-variable -scope 1 -name menuexit -value $true
                    }
                    update-status
                }
            } 
        ) | out-null
    }
    show-menu -menu $NsxVtepVdsMenu
}

function New-IpPool {

    [string]$response = read-host -prompt "Enter a unique pool name"
    while ( $IpPools.Contains($response)) { 
        "Pool Name already exists.  Specify a unique name."
        [string]$response = read-host -prompt "Enter a unique pool name"
    }

    $CurrentPoolName = $response
    $tempPool = @{
        "Prefix" = $null
        "Gateway" = $null
        "StartIp" = $null
        "EndIp" = $null
        "PrimaryDNS" = $null
        "SecondaryDNS" = $null
        "DomainName"= $null
        "NetworkAddress" = $null
    }

    Do { $response = read-host -prompt "Enter prefix length (1-30)" } 
    while ((1..30) -notcontains $response)
    
    $tempPool.Prefix = $response
    
    #Get the gateway and ensure it is a valid IP
    [ipaddress]$TestIP = $null
    do { 
        $response = read-host -prompt "Enter gateway address"
    } while ( -not ([ipaddress]::TryParse( $response, [ref]$TestIp)))

    $tempPool.Gateway = $response
    $tempPool.NetworkAddress = (Get-NetworkFromHostAddress -Address $response -BitMask $tempPool.Prefix).tostring()
                        
    $testrange = Get-NetworkRange -Network $tempPool.NetworkAddress -bitmask $tempPool.Prefix

    #Get the Starting IP
    [ipaddress]$TestIp = $null
    do { 
        $response = read-host -prompt "Enter pool start IP address in the subnet $($tempPool.NetworkAddress)/$($tempPool.Prefix)"
    } while ( (-not ([ipaddress]::TryParse( $response, [ref]$TestIp) )) -or ( $response -eq $tempPool.Gateway ) -or ( -not $testrange.ValidAddressRange.Contains($response)))

    $tempPool.StartIp = $response.ToString()

    #Get the Ending IP
    [ipaddress]$TestIp = $null
    do { 
        $response = read-host -prompt "Enter pool end IP address in the subnet $($tempPool.NetworkAddress)/$($tempPool.Prefix)"
    } while ( 
        ( -not ([ipaddress]::TryParse( $response, [ref]$TestIp) )) -or 
        ( -not (Test-AddressInNetwork -network $tempPool.NetworkAddress -BitMask $tempPool.Prefix -address $response)) -or 
        ( -not $testrange.ValidAddressRange.Contains($response)) -or 
        (($testrange.ValidAddressRange.IndexOf($tempPool.StartIp) -ge $testrange.ValidAddressRange.IndexOf($response)))
    )

    $tempPool.EndIp = $response.ToString()
    
    if ( ($testrange.ValidAddressRange.IndexOf($tempPool.EndIp) - $testrange.ValidAddressRange.IndexOf($tempPool.StartIp)) -lt 2 ) { 
        write-warning "Specified range has less than 3 usable addresses"
    }
   
     #Get the Primary DNS
    [ipaddress]$TestIp = $null
    do { 
        $response = read-hostwithdefault -default $DnsServer1 -prompt "Enter pool Primary DNS Server"
    } while ( -not ([ipaddress]::TryParse( $response, [ref]$TestIp) ))

    $tempPool.PrimaryDNS = $response.ToString()

    #Get the Secondary DNS
    [ipaddress]$TestIp = $null
    do { 
        $response = read-hostwithdefault -default $DnsServer2 -prompt "Enter pool Secondary DNS Server"
    } while ( -not ([ipaddress]::TryParse( $response, [ref]$TestIp) )) 

    $tempPool.SecondaryDNS = $response.ToString()

    #Get the pool domain name
    do { 
        [string]$response = read-hostwithdefault -default $DnsDomain -prompt "Enter a valid DNS Domain Suffix"
    } while ( $response -notmatch $domain_regex )

    $tempPool.DomainName = $response.ToString()

    $script:IpPools.Add($CurrentPoolName, $TempPool) | out-null
    $CurrentPoolName
}

function Select-NsxIpPoolMenu { 

    $script:IpPoolMenu = @{ 
        "Script" = {};
        "Enabled" = $true;
        "Name" = "Select Ip Pool for $SelectedClusterName VTEP addressing";
        "HelpText" = "Create or select IP Pool for VTEP Addressing.  Valid vCenter configuration required"
        "Items" = New-Object System.Collections.Arraylist
    }

    $script:IpPoolMenu.Items.Add(
        @{ 
            "Name" = "Create New Ip Pool";
            "Enabled" = $true
            "Selectable" = $true
            "Selected" = $false
            "Script" = { 
                $NewPool = New-IpPool

                #We have to add the newly created pool to the existing menu...
                $script:IpPoolMenu.Items.Add(
                    @{ 
                        "Name" = $NewPool
                        "Enabled" = $true
                        "Selectable" = $true
                        "Selected" = ($Clusters.$SelectedClusterMoRef.ClusterVxlanIpPool -eq $newpool)
                        "ValidData" = ($Clusters.$SelectedClusterMoRef.ClusterVxlanIpPool -eq $newpool)
                        "Script" = { 
                            $script:Clusters.$SelectedClusterMoRef.ClusterVxlanIpPool = $ipPoolMenu.Items[$SelectedItemNumber].Name
                            $script:Clusters.$SelectedClusterMoRef.ClusterVxlanDhcp = $False
                            set-variable -scope 1 -name menuexit -value $true 
                        }
                    } 
                ) | out-null
            }
        }
    ) | out-null

    foreach ( $ippool in $IpPools.Keys ) {
        $script:IpPoolMenu.Items.Add(
            @{ 
                "Name" = $IpPool
                "Enabled" = $true
                "Selectable" = $true
                "Selected" = ($Clusters.$SelectedClusterMoRef.ClusterVxlanIpPool -eq $ippool)
                "ValidData" =  ($Clusters.$SelectedClusterMoRef.ClusterVxlanIpPool -eq $ippool)
                "Script" = { 
                    $script:Clusters.$SelectedClusterMoRef.ClusterVxlanIpPool = $ipPoolMenu.Items[$SelectedItemNumber].Name
                    $script:Clusters.$SelectedClusterMoRef.ClusterVxlanDhcp = $False
                    set-variable -scope 1 -name menuexit -value $true 
                }
            } 
        ) | out-null
    }
    
    show-menu -menu $IpPoolMenu | out-null
}

function Select-NsxVtepAddressingMenu { 
    
    $script:NsxVtepAddressingMenu = @{ 
        "Script" = {};
        "Enabled" = $false;
        "Name" = "Configure VTEP Addressing";
        "HelpText" = "Choose between DHCP or IP Pool based VTEP addressing.  Valid vCenter connection required"
        "Items" = @(
            @{ 
                "Name" = "Use DHCP for VTEP addressing";
                "Selectable" = $true;
                "Selected" = $clusters.$SelectedClusterMoRef.ClusterVxlanDhcp;
                "ValidData" = $clusters.$SelectedClusterMoRef.ClusterVxlanDhcp;
                "Enabled" = ( -not ( $Clusters.$SelectedClusterMoRef.ClusterVxlanIpPool ));
                "Script" = { 
                    if ( -not $NsxVtepAddressingMenu.Items[0].Selected ) { 
                        #Set me as 'selected' and disable IP Pool config
                        $script:NsxVtepAddressingMenu.Items[0].Selected = $true
                        $script:NsxVtepAddressingMenu.Items[0].ValidData = $true
                        $script:NsxVtepAddressingMenu.Items[1].Enabled = $false
                        $script:Clusters.$SelectedClusterMoRef.ClusterVxlanDhcp = $true
                    }
                    else {
                        #unset me as selected and enabble ip pool config
                        $script:NsxVtepAddressingMenu.Items[0].Selected = $false
                        $script:NsxVtepAddressingMenu.Items[0].ValidData = $false
                        $script:NsxVtepAddressingMenu.Items[1].Enabled = $true
                        $script:Clusters.$SelectedClusterMoRef.ClusterVxlanDhcp = $false
                    }
                }      
            },
            @{ 
                "Name" = "Use IP Pool for VTEP addressing";
                "Enabled" = ( -not ( $Clusters.$SelectedClusterMoRef.ClusterVxlanDhcp ));
                "Selected" = $clusters.$SelectedClusterMoRef.ClusterVxlanIpPool;
                "ValidData" = $clusters.$SelectedClusterMoRef.ClusterVxlanIpPool;
                "CompletedItemText" = $clusters.$SelectedClusterMoRef.ClusterVxlanIpPool;
                "Script" = { 

                    #unset me as selected and enabble ip pool config
                    $script:NsxVtepAddressingMenu.Items[0].Selected = $false
                    $script:NsxVtepAddressingMenu.Items[0].ValidData = $false
                    $script:NsxVtepAddressingMenu.Items[0].Enabled = $false
                    $script:Clusters.$SelectedClusterMoRef.ClusterVxlanDhcp = $false                
                    Select-NsxIpPoolMenu; update-status}
            }
        )
    }

    show-menu -menu $NsxVtepAddressingMenu
}

function Get-NsxClusterConfigMenu { 
    
    $script:NsxClusterConfigMenu= @{ 
        "Script" = {};
        "Enabled" = $false;
        "Name" = "Configure NSX Prepared Cluster";
        "HelpText" = "Valid vCenter connection required"
        "Items" = @(
            @{ 
                "Name" = "Prepare Cluster for NSX"; 
                "Enabled" = $true; 
                "Selectable"=$true; 
                "Selected" = $Clusters.Contains($SelectedClusterMoRef);
                "ValidData" = $Clusters.Contains($SelectedClusterMoRef);
                "Script" = { 
                    if ( -not $NsxClusterConfigMenu.Items[0].Selected ) { 

                        $script:Clusters.Add(
                            $SelectedClusterMoRef,
                            @{
                                "ClusterName" = $SelectedClusterName;
                                "ClusterVds" = $null;
                                "ClusterVxlanDhcp" = $null;
                                "ClusterVxlanIpPool" = $null;
                            }
                        ) | out-null
                        update-status
                    }
                    else {

                        $script:Clusters.Remove( $SelectedClusterMoRef )
                        update-status
                    }
                }      
            },
            @{ 
                "Name" = "Select NSX Prepared VDS for Cluster"; 
                "Enabled" = $Clusters.Contains($SelectedClusterMoRef);
                "ValidData" = $clusters.$SelectedClusterMoRef.ClusterVDS;
                "Script" = { 
                    Select-NsxVtepVdsMenu 
                    update-status
                }      
            },
            @{ 
                "Name" = "Configure VTEP Addressing for Cluster"; 
                "Enabled" = $clusters.$SelectedClusterMoRef.ClusterVDS;
                "ValidData" = ( $clusters.$SelectedClusterMoRef.ClusterVxlanDhcp -or $clusters.$SelectedClusterMoRef.ClusterVxlanIpPool );
                "Script" = { 
                    Select-NsxVtepAddressingMenu
                    update-status 
                }      
            }              
        )
    }

    show-menu -menu $NsxClusterConfigMenu 
}

function Select-NsxClusterMenu { 
    
    $script:NsxClustersMenu= @{ 
        "Script" = {};
        "Enabled" = $false;
        "Name" = "Select Nsx Cluster";
        "HelpText" = "Valid vCenter connection required"
        "Items" = New-Object System.Collections.Arraylist
    }

    foreach ( $cluster in Get-Cluster -server $vCenterConnection ) {
        $script:NsxClustersMenu.Items.Add( 
            @{ 
                "Name" = $cluster.name
                "MoRef" = $Cluster.Id
                "Selectable" = $true
                "Selected" = $Clusters.Contains($cluster.Id)
                "ValidData" = ( $Clusters.Contains($cluster.Id) -and $clusters.($cluster.Id).ClusterVds -and ($clusters.($cluster.Id).ClusterVxlanDhcp -or $clusters.($cluster.Id).ClusterVxlanIpPool ))
                "Enabled" = $true
                "Script" = { 

                    $SelectedClusterName = $NsxClustersMenu.Items[$SelectedItemNumber].name
                    $SelectedClusterMoRef = $NsxClustersMenu.Items[$SelectedItemNumber].MoRef
                    Get-NsxClusterConfigMenu
                    update-Status    
                }
            }
        ) | out-null
    }
    show-menu -menu $NsxClustersMenu
}

function Update-Status {

    #Called when menu items status need updating.

    $script:Configuration.ValidData = ( $vSphereConfiguration.ValidData -and $NsxConfiguration.ValidData -and $NetworkConfiguration.ValidData )

    #vSphereConfiguration, Network configuration, NSX configuration
    $script:vSphereConfiguration.ValidData = ( $vCenterConnection -and $MgmtClusterName -and $ManagementDatastoreName -and $MgmtPgName )
    $script:NetworkConfiguration.ValidData = ( $NtpServer -and $DnsServer1 -and $DnsServer2 -and $DnsDomain )
    $script:NsxConfiguration.Enabled = ( $vSphereConfiguration.ValidData -and $NetworkConfiguration.ValidData )

    $script:NSXConfiguration.ValidData = ( $NsxManagerOVF -and $NSXManagerName -and $NsxManagerPassword -and $ControllerPassword -and $TransportZoneName -and $SegmentPoolStart -and $SegmentPoolEnd -and $VxlanMtuSize -and ($clusters.count -ge 1) )


    #Update vsphereconfig menu items selecteditemtext 
    #vc Connection
    $script:vSphereConfiguration.Items[0].ValidData = $vCenterConnection
    $script:vSphereConfiguration.items[0].CompletedItemText = $vCenterServer

    #ManagementCluster
    $script:vSphereConfiguration.Items[1].Enabled = $vCenterConnection
    $script:vSphereConfiguration.Items[1].ValidData = $MgmtClusterName
    $script:vSphereConfiguration.items[1].CompletedItemText = $MgmtClusterName

    #ManagementDatastoreName
    $script:vSphereConfiguration.Items[2].Enabled = $vCenterConnection
    $script:vSphereConfiguration.Items[2].ValidData = $ManagementDatastoreName
    $script:vSphereConfiguration.items[2].CompletedItemText = $ManagementDatastoreName

    #ManagementPortGroupName
    $script:vSphereConfiguration.Items[3].Enabled = $vCenterConnection
    $script:vSphereConfiguration.Items[3].ValidData = $MgmtPgName
    $script:vSphereConfiguration.items[3].CompletedItemText = $MgmtPgName


    #Update NSXConfig Menu items SelectedItemText
    $script:NSXConfiguration.Items[0].ValidData = $NsxManagerOVF
    $script:NSXConfiguration.items[0].CompletedItemText = $NsxManagerOVF

    $script:NSXConfiguration.items[1].ValidData = $NSXManagerName
    $script:NSXConfiguration.items[1].CompletedItemText = $NSXManagerName
    
    $script:NSXConfiguration.items[4].ValidData = $TransportZoneName
    $script:NSXConfiguration.items[4].CompletedItemText = $TransportZoneName

    $script:NSXConfiguration.items[5].ValidData = ($SegmentPoolStart -and $SegmentPoolEnd)
    $script:NSXConfiguration.items[5].CompletedItemText = "$SegmentPoolStart-$SegmentPoolEnd"

    $script:NSXConfiguration.items[6].ValidData = $VxlanMtuSize
    $script:NSXConfiguration.items[6].CompletedItemText = $VxlanMtuSize

    $script:NSXConfiguration.items[7].ValidData = $NsxManagerIpAddress
    $script:NSXConfiguration.items[8].ValidData = ($Clusters.count -ge 1)


    #Update Network Config Menu Items...
    $script:NetworkConfiguration.items[0].CompletedItemText = $SyslogServer
    $script:NetworkConfiguration.items[0].ValidData = $SyslogServer
    $script:NetworkConfiguration.items[1].CompletedItemText = $SyslogPort
    $script:NetworkConfiguration.items[1].ValidData = $SyslogPort
    $script:NetworkConfiguration.items[2].CompletedItemText = $SyslogProtocol
    $script:NetworkConfiguration.items[2].ValidData = $SyslogProtocol
    $script:NetworkConfiguration.items[3].CompletedItemText = $NtpServer
    $script:NetworkConfiguration.items[3].ValidData = $NtpServer
    $script:NetworkConfiguration.items[4].CompletedItemText = $DnsServer1
    $script:NetworkConfiguration.items[4].ValidData = $DnsServer1
    $script:NetworkConfiguration.items[5].CompletedItemText = $DnsServer2
    $script:NetworkConfiguration.items[5].ValidData = $DnsServer2
    $script:NetworkConfiguration.items[6].CompletedItemText = $DnsDomain
    $script:NetworkConfiguration.items[6].ValidData = $DnsDomain

    

    #nsx -> Management Network Addressing
    if ( $ManagementNetworkMenu ) { 
        $script:ManagementNetworkMenu.Items[0].ValidData = $ManagementIpPool
        $script:ManagementNetworkMenu.Items[0].CompletedItemText = $ManagementIpPool

        if ( $ManagementNetworkMenu.Items[0].ValidData ) { 
            $script:ManagementNetworkMenu.Items[1].Enabled = $true
        }
        $script:ManagementNetworkMenu.Items[1].CompletedItemText = $NsxManagerIpAddress
        $script:ManagementNetworkMenu.Items[1].ValidData = $NsxManagerIpAddress

    }

    #nsx -> cluster selection
    foreach ( $item in $script:NsxClustersMenu.items ) { 
        $Item.Selected = $clusters.Contains($item.MoRef)
        $Item.ValidData = ( $clusters.($Item.Moref).ClusterVds -and ($clusters.($Item.Moref).ClusterVxlanDhcp -or $clusters.($Item.Moref).ClusterVxlanIpPool )) 
    }

    #Current cluster config menu - > select vds and addressing menus...
    if ( $NSxClusterConfigMenu -and $SelectedClusterMoRef ) { 
        $script:NsxClusterConfigMenu.items[0].selected = $script:NsxClusterConfigMenu.items[0].validdata =  $clusters.contains($SelectedClusterMoRef)

        #VDS Selection menu item
        $script:NsxClusterConfigMenu.items[1].enabled = $clusters.contains($SelectedClusterMoRef)
        $script:NsxClusterConfigMenu.items[1].ValidData = $clusters.$SelectedClusterMoRef.ClusterVDS
        if ( $Clusters.$SelectedClusterMoRef.ClusterVds) { 
            $script:NsxClusterConfigMenu.items[1].CompletedItemText = $nsxvds.$($Clusters.$SelectedClusterMoRef.ClusterVds).vdsName
        }
        #Addressing menu item
        $script:NsxClusterConfigMenu.items[2].enabled = $clusters.contains($SelectedClusterMoRef)
        $script:NsxClusterConfigMenu.Items[2].ValidData = ( $clusters.$SelectedClusterMoRef.ClusterVxlanDhcp -or $clusters.$SelectedClusterMoRef.ClusterVxlanIpPool )

        if ( $Clusters.$SelectedClusterMoRef.ClusterVxlanDhcp ) { 
            $script:NsxClusterConfigMenu.Items[2].CompletedItemText = "DHCP" 
        }
        else {
            $script:NsxClusterConfigMenu.Items[2].CompletedItemText = $Clusters.$SelectedClusterMoRef.ClusterVxlanIpPool  
        }
    }

    #Vtep Addressing Menu
    if ( $NsxVtepAddressingMenu -and $SelectedClusterMoRef ) { 
        $script:NsxVtepAddressingMenu.Items[1].Selected = $clusters.$SelectedClusterMoRef.ClusterVxlanIpPool
        $script:NsxVtepAddressingMenu.Items[1].ValidData = $clusters.$SelectedClusterMoRef.ClusterVxlanIpPool
        $script:NsxVtepAddressingMenu.Items[1].CompletedItemText = $clusters.$SelectedClusterMoRef.ClusterVxlanIpPool
    }

    $script:Rootmenu.Items[1].Enabled = $vSphereConfiguration.ValidData -and $NetworkConfiguration.ValidData -and $NsxConfiguration.ValidData

    $script:PocOptions.ValidData = ( -not ( $SingleControllerCluster -or $12GbManager ))
    $script:PocOptions.Items[0].Selected = $SingleControllerCluster
    $script:PocOptions.Items[0].ValidData = $SingleControllerCluster
    $script:PocOptions.Items[1].Selected = $12GbManager
    $script:PocOptions.Items[1].ValidData = $12GbManager
}


function ConvertTo-PSON {
    <#
    .SYNOPSIS
    creates a powershell object-notation script that generates the same object data
    .DESCRIPTION
    This produces 'PSON', the powerShell-equivalent of JSON from any object you pass to it. It isn't suitable for the huge objects produced by some of the cmdlets such as Get-Process, but fine for simple objects
    .EXAMPLE
    $array=@()
    $array+=Get-Process wi* |  Select-Object Handles,NPM,PM,WS,VM,CPU,Id,ProcessName 
    ConvertTo-PSON $array

    .PARAMETER Object 
    the object that you want scripted out
    .PARAMETER Depth
    The depth that you want your object scripted to
    .PARAMETER Nesting Level
    internal use only. required for formatting
    #>
    
    #Been looking for this for some time ;)
    #From https://raw.githubusercontent.com/Phil-Factor/ConvertToPSON/master/ConvertTo-PSON.ps1

    [CmdletBinding()]
    param (
        [parameter(Position = 0, Mandatory = $true, ValueFromPipeline = $true)]
        [AllowNull()]
        $inputObject,
        [parameter(Position = 1, Mandatory = $false, ValueFromPipeline = $false)]
        [int]$depth = 16,
        [parameter(Position = 2, Mandatory = $false, ValueFromPipeline = $false)]
        [int]$NestingLevel = 1,
        [parameter(Position = 3, Mandatory = $false, ValueFromPipeline = $false)]
        [int]$XMLAsInnerXML = 0
    )
    
    BEGIN { }
    PROCESS
    {
        If ($inputObject -eq $Null) { $p += '$Null'; return $p } # if it is null return null
        $padding = [string]'  ' * $NestingLevel # lets just create our left-padding for the block
        $ArrayEnd = 0; #until proven false
        try
        {
            $Type = $inputObject.GetType().Name # we start by getting the object's type
            if ($Type -ieq 'Object[]') { $Type = "$($inputObject.GetType().BaseType.Name)" } # see what it really is
            if ($depth -ilt $NestingLevel) { $Type = 'OutOfDepth' } #report the leaves in terms of object type
            elseif ($Type -ieq 'XmlDocument' -or $Type -ieq 'XmlElement')
            {
                if ($XMLAsInnerXML -ne 0) { $Type = 'InnerXML' }
                else
                { $Type = 'XML' }
            } # convert to PS Alias
            # prevent these values being identified as an object
            if (@('boolean', 'byte', 'char', 'datetime', 'decimal', 'double', 'float', 'single', 'guid', 'int', 'int32',
            'int16', 'long', 'int64', 'OutOfDepth', 'RuntimeType', 'PSNoteProperty', 'regex', 'sbyte', 'string',
            'timespan', 'uint16', 'uint32', 'uint64', 'uri', 'version', 'void', 'xml', 'datatable', 'Dictionary`2'
            'SqlDataReader', 'datarow', 'ScriptBlock', 'type') -notcontains $type)
            {
                if ($Type -ieq 'OrderedDictionary') { $Type = 'HashTable' }
                elseif ($Type -ieq 'List`1') { $Type = 'Array' }
                elseif ($Type -ieq 'PSCustomObject') { $Type = 'PSObject' } #
                elseif ($inputObject -is "Array") { $Type = 'Array' } # whatever it thinks it is called
                elseif ($inputObject -is "HashTable") { $Type = 'HashTable' } # for our purposes it is a hashtable
                elseif ($inputObject -is "Generic") { $Type = 'DotNotation' } # for our purposes it is a hashtable
                #elseif ((gm -inputobject $inputObject -membertype Methods | Select name|where name -like 'GetEnumerator') -ne $null) { $Type = 'HashTable' }
                elseif (($inputObject | gm -membertype Properties | Select name | Where name -like 'Keys') -ne $null) { $Type = 'DotNotation' } #use dot notation
                elseif (($inputObject | gm -membertype Properties | Select name).count -gt 1) { $Type = 'Object' }
            }
            write-verbose "$($padding)Type:='$Type', Object type:=$($inputObject.GetType().Name), BaseName:=$($inputObject.GetType().BaseType.Name) $NestingLevel "
            switch ($Type)
            {
                'ScriptBlock'{ "[$type] {$($inputObject.ToString())}" }
                'InnerXML'        { "[$type]@'`r`n" + ($inputObject.OuterXMl) + "`r`n'@`r`n" } # just use a 'here' string
                'DateTime'   { "[datetime]'$($inputObject.ToString('s'))'" } # s=SortableDateTimePattern (based on ISO 8601) local time
                'Boolean'    {
                    "[bool] $(&{
                        if ($inputObject -eq $true) { "`$True" }
                        Else { "`$False" }
                    })"
                }
                'string'     {
                    if ($inputObject -match '[\r\n]') { "@'`r`n$inputObject`r`n'@" }
                    else { "'$($inputObject -replace '''', '''''')'" }
                }
                'Char'       { [int]$inputObject }
                { @('byte', 'decimal', 'double', 'float', 'single', 'int', 'int32', 'int16', 'long', 'int64', 'sbyte', 'uint16', 'uint32', 'uint64') -contains $_ }
                { "$inputObject" } # rendered as is without single quotes
                'PSNoteProperty' { "$(ConvertTo-PSON -inputObject $inputObject.Value -depth $depth -NestingLevel ($NestingLevel))" }
                'Array'      { "`r`n$padding@(" + ("$($inputObject | ForEach { $ArrayEnd = 1; ",$(ConvertTo-PSON -inputObject $_ -depth $depth -NestingLevel ($NestingLevel + 1))" })".Substring($ArrayEnd)) + "`r`n$padding)" }
                'HashTable'  { "`r`n$padding@{" + ("$($inputObject.GetEnumerator() | ForEach { $ArrayEnd = 1; "; '$($_.Name)' = " + (ConvertTo-PSON -inputObject $_.Value -depth $depth -NestingLevel ($NestingLevel + 1)) })".Substring($ArrayEnd) + "`r`n$padding}") }
                'PSObject'   { "`r`n$padding[pscustomobject]@{" + ("$($inputObject.PSObject.Properties | ForEach { $ArrayEnd = 1; "; '$($_.Name)' = " + (ConvertTo-PSON -inputObject $_ -depth $depth -NestingLevel ($NestingLevel + 1)) })".Substring($ArrayEnd) + "`r`n$padding}") }
                'Dictionary' { "`r`n$padding@{" + ($inputObject.item | ForEach { $ArrayEnd = 1; '; ' + "'$_'" + " = " + (ConvertTo-PSON -inputObject $inputObject.Value[$_] -depth $depth -NestingLevel $NestingLevel+1) }) + '}' }
                'DotNotation'{ "`r`n$padding@{" + ("$($inputObject.Keys | ForEach { $ArrayEnd = 1; ";  $_ =  $(ConvertTo-PSON -inputObject $inputObject.$_ -depth $depth -NestingLevel ($NestingLevel + 1))" })".Substring($ArrayEnd) + "`r`n$padding}") }
                'Dictionary`2'{ "`r`n$padding@{" + ("$($inputObject.GetEnumerator() | ForEach { $ArrayEnd = 1; "; '$($_.Key)' = " + (ConvertTo-PSON -inputObject $_.Value -depth $depth -NestingLevel ($NestingLevel + 1)) })".Substring($ArrayEnd) + "`r`n$padding}") }
                'Object'     { "`r`n$padding@{" + ("$($inputObject | Get-Member -membertype properties | Select-Object name | ForEach { $ArrayEnd = 1; ";  $($_.name) =  $(ConvertTo-PSON -inputObject $inputObject.$($_.name) -depth $NestingLevel -NestingLevel ($NestingLevel + 1))" })".Substring($ArrayEnd) + "`r`n$padding}") }
                'XML'        { "`r`n$padding@{" + ("$($inputObject | Get-Member -membertype properties | where name -ne 'schema' | Select-Object name | ForEach { $ArrayEnd = 1; ";  $($_.name) =  $(ConvertTo-PSON -inputObject $inputObject.$($_.name) -depth $depth -NestingLevel ($NestingLevel + 1))" })".Substring($ArrayEnd) + "`r`n$padding}") }
                'Datatable'  { "`r`n$padding@{" + ("$($inputObject.TableName)=`r`n$padding @(" + "$($inputObject | ForEach { $ArrayEnd = 1; ",$(ConvertTo-PSON -inputObject $_ -depth $depth -NestingLevel ($NestingLevel + 1))" })".Substring($ArrayEnd) + "`r`n$padding  )`r`n$padding}") }
                'DataRow'    { "`r`n$padding@{" + ("$($inputObject | Get-Member -membertype properties | Select-Object name | ForEach { $ArrayEnd = 1; "; $($_.name)=  $(ConvertTo-PSON -inputObject $inputObject.$($_.name) -depth $depth -NestingLevel ($NestingLevel + 1))" })".Substring($ArrayEnd) + "}") }
                default { "'$inputObject'" }
            }
        }
        catch
        {
            write-error "Error'$($_)' in script $($_.InvocationInfo.ScriptName) $($_.InvocationInfo.Line.Trim()) (line $($_.InvocationInfo.ScriptLineNumber)) char $($_.InvocationInfo.OffsetInLine) executing $($_.InvocationInfo.MyCommand) on $type object '$($inputObject.Name)' Class: $($inputObject.GetType().Name) BaseClass: $($inputObject.GetType().BaseType.Name) "
        }
        finally { }
    }
    END { }
}


function save-config {
    
    $configfile = 'config.ps1'
    if ( test-path $configfile ) { 

        $message  = "Config File Exists."
        $question = "Overwrite?"
        $decision = $Host.UI.PromptForChoice($message, $question, $yesnochoices, 1)
    
    }    
    else { $decision = 0 } 
    if ($decision -eq 0) {

        $outobj = [pscustomobject]@{
        
            '_config' = get-date -format yyyy_M_d_HH_mm
            'vCenterServer' = $vCenterServer
            'MgmtClusterName' = $MgmtClusterName
            'ManagementDatastoreName' = $ManagementDatastoreName
            'MgmtPgName' = $MgmtPgName
            'NsxManagerOVF' = $NsxManagerOVF
            'NSXManagerName' = $NSXManagerName
            'TransportZoneName' = $TransportZoneName
            'SegmentPoolStart' = $SegmentPoolStart 
            'SegmentPoolEnd' = $SegmentPoolEnd
            'NsxManagerIpAddress' = $NsxManagerIpAddress
            'SyslogServer' = $SyslogServer
            'SyslogPort' = $SyslogPort
            'SyslogProtocol' = $SyslogProtocol
            'NtpServer' = $NtpServer
            'DnsServer1' = $DnsServer1
            'DnsServer2' = $DnsServer2
            'DnsDomain' = $DnsDomain
            'ManagementIpPool' = $ManagementIpPool
            'VxlanMtuSize' = $VxlanMtuSize
            'Clusters' = $Clusters
            'IpPools' = $IpPools 
            'NsxVds' = $NsxVds
        }
        
        $outobj | convertto-pson | set-content $configfile
        "Saved Config to $configfile"
    }
}

function read-config {

    $configfile = '.\config.ps1'

    if ( -not (test-path $configfile) ) { 

        "Config file does not exist"
    }
    else {

        try { 

            set-strictmode -Version Latest
            $inobj =  & $configfile
            $script:vCenterServer = $inobj.vCenterServer
            $script:MgmtClusterName = $inobj.MgmtClusterName
            $script:ManagementDatastoreName = $inobj.ManagementDatastoreName
            $script:MgmtPgName = $inobj.MgmtPgName
            $script:NsxManagerOVF = $inobj.NsxManagerOVF
            $script:NSXManagerName = $inobj.NSXManagerName
            $script:TransportZoneName = $inobj.TransportZoneName
            $script:SegmentPoolStart = $inobj.SegmentPoolStart 
            $script:SegmentPoolEnd = $inobj.SegmentPoolEnd
            $script:NsxManagerIpAddress = $inobj.NsxManagerIpAddress
            $script:SyslogServer = $inobj.SyslogServer
            $script:SyslogPort = $inobj.SyslogPort
            $script:SyslogProtocol = $inobj.SyslogProtocol
            $script:NtpServer = $inobj.NtpServer
            $script:DnsServer1 = $inobj.DnsServer1
            $script:DnsServer2 = $inobj.DnsServer2
            $script:DnsDomain = $inobj.DnsDomain
            $script:ManagementIpPool = $inobj.ManagementIpPool
            $script:VxlanMtuSize = $inobj.VxlanMtuSize
            $script:Clusters = $inobj.Clusters
            $script:IpPools = $inobj.IpPools 
            $script:NsxVds = $inobj.NsxVds
            "Loaded from Config $($inobj._config) in file $configfile"
        }
        catch {

            throw "An error occured loading config from $configfile.  $_"
        }
    }

}

function show-menu { 


    <#
    .SYNOPSIS
    Displays a menu that provides nesting of menu items, arbitrary script
    block execution, status handling input handling

    .DESCRIPTION
    Provides a simple template based menu function.  The user defines a series
    of 'MenuItems' - hashtables of an expected format (see examples for detail)
    - that allow a menu of choices consisting of nested menus, or individual 
    items that execute an arbitrary scriptblock to be shown to the user.

    Menuitems have an enabled property that can be toggled at runtime, 
    allowing code executed in one menuitem to influence another.

    Simple Header and subheader value and color properties allow the menu to
    be customised at runtime, and a footer property allows the user to define 
    a scriptblock that returns a string that is executed every time the menu is 
    redrawn that allows for a simple feedback loop of 'state' if required.

    .EXAMPLE

    #Defines a simple menu consisting of a Header and subheader

    $MainHeader = "Menu Test Header"
    $Subheader = "Menu Test SubHeader"

    $Configuration = @{ 
        "Script" = { show-menu -menu $Configuration | out-null };
        "Enabled" = $true;
        "Name" = "Configuration";
        "HelpText" = "Nothing"
        "Items" = @( 
            @{
                "Enabled" = $true;
                "Name" = "Enable Processing";
                "Script" = { 
                    $Processing.Enabled = $true
                    $script:Configured = $true
                    return "Processing Enabled"
                } 
            }
        )
    }

    $Processing = @{ 
        "Script" = { show-menu -menu $Processing | out-null };
        "Enabled" = $false;
        "Name" = "Processing";
        "HelpText" = "Need to enable me first"
        "Items" = @( 
            @{
                "Enabled" = $true;
                "Name" = "get-date";
                "Script" = { $script:CurrentDate = get-date } 
            }
        )
    }

    $rootmenu = @{ 
        "Name" = "Root Menu";
        "Items" = @( 
            $Configuration,
            $Processing
        )
    }

    $Footer = { 
        "Configured : $($Configured)`nCurrent Date : $($CurrentDate)"
    }

    show-menu -menu $rootmenu

    #>

    #Show-Menu from https://bitbucket.org/nbradford/misc/src/9c46ff32a8d7accf0cc49cd2066f74102e1465a3/PowerShell/util.psm1?at=master&fileviewer=file-view-default

    param (
        [string]$MainHeader=$MainHeader,
        [string]$Subheader=$Subheader,
        [scriptblock]$Footer=$Footer,
        [hashtable]$menu,
        [string]$HeaderColor="Green",
        [string]$SubheaderColor="Green",
        [string]$MenuItemColor="White",
        [string]$FooterColor="Green",
        [string]$MenuTitleColor="Yellow",
        [string]$MenuEnabledItemColor="White",
        [string]$MenuDisabledItemColor="DarkGray",
        [string]$MenuItemSelectedColor="DarkGreen",
        [string]$MenuItemIncompleteColor="Yellow",
        [string]$PromptColor="Yellow"

    )

    if ( $memu.items.count -gt 10 ) { throw "Too many items in menu - whinge at bradford. "}


    $keyvalid = $false
    $status = ""
    $statuscolor = "white"
    if ( -not $script:breadcrumb ) {
        $script:breadcrumb = New-Object System.Collections.Arraylist
    }
    $script:breadcrumb.add($menu.Name) | out-null

    while ( -not $menuexit ) { 

        $ConsoleWidth = (Get-host).ui.RawUI.windowsize.width

        clear-host
        write-host -foregroundcolor $HeaderColor ( "*" * $ConsoleWidth )  
        write-host -foregroundcolor $HeaderColor $MainHeader
        if ( $subheader) { 
            write-host -foregroundcolor $SubheaderColor $subheader 
        }
        write-host -foregroundcolor $HeaderColor ( "*" * $ConsoleWidth + "`n" )  
        write-host -foregroundcolor $MenuTitleColor "$($breadcrumb -join(" > "))`n"
        for ( $index=0; $index -lt ($menu.items.count ); $index++ ) { 

            $ItemText = $menu.items[$index].Name
            $Column2Width = 40
            $Column1Width = $ConsoleWidth - $Column2Width - 10

            if ( -not $menu.items[$index].Enabled ) { 
                write-host -foregroundcolor $MenuDisabledItemColor $("{0,-$Column1Width }{1,-$Column2Width}" -f "$($index + 1) - $ItemText"," [  DISABLED  ]")

            }
            elseif ( -not $menu.items[$index].Selectable ) {
                if ( -not $menu.items[$index].ValidData ) { 
                    #Flag the menu item as needing input
                    write-host -foregroundcolor $MenuItemIncompleteColor $("{0,-$Column1Width} {1,-$Column2Width}" -f "$($index + 1) - $ItemText","[ INCOMPLETE ]")
                }
                else {
                    #Normal complete item
                    if ( $menu.items[$index].CompletedItemText ) { 
                        $CompletedItemText = $menu.items[$index].CompletedItemText
                    }
                    else { 
                        $CompletedItemText = "COMPLETE"
                    }
                    write-host -foregroundcolor $MenuItemColor $("{0,-$Column1Width} {1,-$Column2Width}" -f "$($index + 1) - $ItemText","[  $CompletedItemText  ]")
                }
            }
            else {
                #Selectable item...
                if ( $menu.items[$index].Selected ) {
                    if ( -not $menu.items[$index].ValidData ) {
                        #Incomplete item
                        write-host -foregroundcolor $MenuItemIncompleteColor $("{0,-$Column1Width} {1,-$Column2Width}" -f "$($index + 1) - $ItemText","[ INCOMPLETE ]") 
                    }
                    else { 
                        #Completed item
                        write-host -foregroundcolor $MenuItemSelectedColor $("{0,-$Column1Width} {1,-$Column2Width}" -f "$($index + 1) - $ItemText","[  SELECTED  ]") 
                    }
                }
                else {
                    #Not selected (but enabled)
                    write-host -foregroundcolor $MenuItemColor $("{0,-$Column1Width} {1,-$Column2Width}" -f "$($index + 1) - $ItemText","[ UNSELECTED ]") 
                }
            }
        } 

        if ( $footer ) { 
            $FooterString = &$footer

            write-host -foregroundcolor $FooterColor ( "`n" + "*" * $ConsoleWidth )
            write-host -foregroundcolor $FooterColor $Footerstring
            write-host -foregroundcolor $FooterColor ( "*" * $ConsoleWidth + "`n" )
        }

        write-host -foregroundcolor $PromptColor "Enter the number of your choice, x to go back or q to quit."
        write-host -foregroundcolor $statuscolor $status

        $key = [console]::ReadKey($true) 
        switch ( $key.keychar.toString() ) {
            "x" { $menuexit = $true }
            "q" { $script:allmenuexit = $true }
            default {
                if ( ( [int]$_ -ge 1 ) -and ( [int]$_ -le ($menu.items.count ))) { 
                    #Valid selection
                    if ( $menu.items[$($_ - 1)].Enabled ) { 
                        $script:SelectedItemNumber = [int]($_ - 1)
                        $status = &$menu.items[$($_ - 1)].script
                        $statuscolor = "White"
                    }
                    else { 
                        $statuscolor = "Yellow"
                        $status = "Option not available. $($menu.items[$($_ - 1)].HelpText)"

                    }
                }
                else { 
                    $statuscolor = "Red"
                    $status = "Invalid choice $_"
                }
            }
        }

        if ( $script:allmenuexit ) { return }
    }

    $script:breadcrumb.RemoveAt($breadcrumb.count - 1 )

}

###############################################
# Define and display menu


$Clusters = @{}
$IpPools = @{}
$NsxVds = @{}


$ConfigurationComplete = $false
$vSphereConfigurationComplete = $false

#Setup the menu system.
$MainHeader = "NSX for vSphere Guided Installation"
$Subheader = "v$version"


#############

$vSphereConfiguration = @{ 
    "Script" = { 
        $footer = {
            "vSphere Configuration Complete: $($vSphereConfiguration.ValidData.tostring())`nNSX Configuration Complete: $($NsxConfiguration.ValidData.ToString())`nNetwork Configuration Complete: $($NetworkConfiguration.ValidData.ToString())"
            update-status
        }
        show-menu -menu $vSphereConfiguration | out-null 
    };
    "Enabled" = $true
    "Name" = "vSphere Configuration"
    "HelpText" = "Nothing"
    "ValidData" = $false
    "Items" = @( 
        @{ 
            "Enabled" = $true
            "Name" = "vCenter Connection"
            "Script" = { Get-vCenterConnectionMenu; Update-Status }
        },
        @{ 
            "Enabled" = $vCenterConnection
            "ValidData" = $MgmtClusterName
            "HelpText" = "Complete vCenter Connection first."
            "Name" = "Select Management Cluster"
            "Script" = { Select-MgmtClusterMenu; Update-Status } 
        },
        @{ 
            "Enabled" = $vCenterConnection
            "ValidData" = $ManagementDatastoreName
            "HelpText" = "Select Management Cluster first."
            "Name" = "Select Management Datastore"
            "Script" = { Select-MgmtDatastoreMenu; Update-Status  } 
        },
        @{ 
            "Enabled" = $vCenterConnection
            "ValidData" = $MgmtPgName
            "HelpText" = "Select Management Cluster first."
            "Name" = "Select Management Distributed PortGroup"
            "Script" = { Select-MgmtPGMenu; Update-Status } 
        }
    )
}

$NsxConfiguration = @{ 
    "Script" = { 
        $footer = {
            "vSphere Configuration Complete: $($vSphereConfiguration.ValidData.tostring())`nNSX Configuration Complete: $($NsxConfiguration.ValidData.ToString())`nNetwork Configuration Complete: $($NetworkConfiguration.ValidData.ToString())"
            update-status
        }
        show-menu -menu $NSXConfiguration | out-null 
    };
    "Enabled" = $false
    "Name" = "NSX Configuration"
    "HelpText" = "Complete vCenter connection and Management Cluster selection first."
    "ValidData" = $false
    "Items" = @( 
        @{ 
            "Enabled" = $true
            "Name" = "NSX Manager OVA File"
            "Script" = { Get-NsxManagerOVA; Update-Status } 
        },
        @{ 
            "Enabled" = $true
            "ValidData" = $NSXManagerName
            "CompletedItemText" = $NsxManagerName
            "Name" = "Set Appliance VM and HostName"
            "Script" = { 
                $ManagerName = read-hostwithdefault -Prompt "Enter Appliance Name" -Default $Default_NsxManagerName
                if ( $ManagerName -match " " ) {    
                    "NSX Manager Appliance name may not contain spaces"
                }
                else {
                    $script:NSXManagerName = $ManagerName
                }
                Update-Status
            }
        },
        @{ 
            "Enabled" = $true
            "ValidData" = $NsxManagerPassword
            "Name" = "Set NSX Manager Appliance Password"
            "Script" = { 
                $Password = Read-host -assecurestring -Prompt "Enter Password" 
                $ConfirmPassword = Read-host -assecurestring -Prompt "Confirm Password" 
                $pwd1 = (New-Object System.Management.Automation.PSCredential 'N/A', $password).GetNetworkCredential().Password
                $pwd2 = (New-Object System.Management.Automation.PSCredential 'N/A', $Confirmpassword).GetNetworkCredential().Password
                If ( ($pwd1 -eq $pwd2) -and $pwd1 -ne "") { 
                    $script:NsxManagerPassword = (New-Object System.Management.Automation.PSCredential 'N/A', $password).GetNetworkCredential().Password
                    $Script:NsxConfiguration.Items[2].ValidData = $true
                }
                else {
                    "Passwords do not match or are empty.  Try again."
                }
            } 
        },
        @{ 
            "Enabled" = $true
            "ValidData" = $ControllerPassword
            "Name" = "Set NSX Controller Password"
            "Script" = { 
                $Password = Read-host -assecurestring -Prompt "Enter Password" 
                $ConfirmPassword = Read-host -assecurestring -Prompt "Confirm Password" 
                $pwd1 = (New-Object System.Management.Automation.PSCredential 'N/A', $password).GetNetworkCredential().Password
                $pwd2 = (New-Object System.Management.Automation.PSCredential 'N/A', $Confirmpassword).GetNetworkCredential().Password
                If ( ($pwd1 -eq $pwd2) -and $pwd1 -ne "") { 
                    $script:ControllerPassword = (New-Object System.Management.Automation.PSCredential 'N/A', $password).GetNetworkCredential().Password
                    $Script:NsxConfiguration.Items[3].ValidData = $true

                }
                else {
                    "Passwords do not match or are empty.  Try again."
                }

            } 
            #Need complexity test in here.
        },
        @{ 
            "Enabled" = $true
            "ValidData" = $TransportZoneName
            "CompletedItemText" = $TransportZoneName
            "Name" = "Set Transport Zone Name"
            "Script" = { 
                $TzName = read-hostwithdefault -Prompt "Enter Transport Zone Name" -Default $Default_TransportZoneName
                if ( $TzName -match " " ) {    
                    "TransportZone name may not contain spaces"
                }
                else {
                    $script:TransportZoneName = $TZName

                }
                Update-Status
            } 
        },
        @{ 
            "Enabled" = $true
            "ValidData" = ($SegmentPoolStart -and $SegmentPoolEnd )
            "CompletedItemText" = "$SegmentPoolStart - $SegmentPoolEnd"
            "Name" = "Set Logical Network Segment ID Pool Range "
            "Script" = { 

                [int]$PoolStart = read-hostwithdefault -Prompt "Enter Pool Start" -Default $Default_SegmentPoolStart
                [int]$PoolEnd = read-hostwithdefault -Prompt "Enter Pool End" -Default $Default_SegmentPoolEnd  
                if ( ($PoolStart -ge $PoolEnd) -or ( $PoolStart -lt 5000 ) -or ($PoolEnd -gt 1677216 ) ) {
                    "Segment Id Pool must be in the range between 5000 and 1677216" 
                }
                else { 
                    $script:SegmentPoolStart = $PoolStart
                    $script:SegmentPoolEnd = $PoolEnd
                }
                Update-Status
            } 
        },
        @{ 
            "Enabled" = $true
            "Name" = "Set VXLAN MTU"
            "ValidData" = $VxlanMtuSize
            "CompletedItemText" = $VxlanMtuSize
            "Script" = { 
                Do { $response = read-hostwithdefault -default $Default_VxlanMtu -prompt "Enter VXLAN MTU" } 
                while ((1600..9000) -notcontains $response)
                $script:VxlanMtuSize = $response
                Update-Status
            }
        },
        @{ 
            "Enabled" = $true;
            "Name" = "Set Mangement Network Details"
            "Script" = { 
                Get-ManagementNetworkMenu
                Update-Status
            } 
        },
        @{ 
            "Enabled" = $true
            "Name" = "Select NSX Enabled Clusters"
            "Script" = { Select-NsxClusterMenu; update-Status } 
        }
    )
}

$NetworkConfiguration = @{
    "Script" = { 
        $footer = {
            "vSphere Configuration Complete: $($vSphereConfiguration.ValidData.tostring())`nNSX Configuration Complete: $($NsxConfiguration.ValidData.ToString())`nNetwork Configuration Complete: $($NetworkConfiguration.ValidData.ToString())"
            update-status
        }
        show-menu -menu $NetworkConfiguration | out-null 
    };
    "Enabled" = $true
    "Name" = "Misc Network Configuration"
    "HelpText" = "Nothing"
    "ValidData" = $false
    "Items" = @( 
        @{ 
            "Enabled" = $true
            "ValidData" = $SyslogServer
            "Selectable" = $false
            "CompletedItemText" = $SyslogServer
            "Name" = "Set Syslog Server (Optional)"
            "Script" = { 

                #Get the response and ensure it is a valid IP
                [ipaddress]$TestIP = $null
                do { 
                    $response = read-host -prompt "Enter Syslog Server ip address"
                } while ( -not ([ipaddress]::TryParse( $response, [ref]$TestIp)))

                $script:SyslogServer = $response
                Update-Status
            }
        },
        @{ 
            "Enabled" = $true;
            "ValidData" = $SyslogPort
            "Selectable" = $false
            "CompletedItemText" = $SyslogPort
            "Name" = "Set Syslog Port (Optional)"
            "Script" = { 

                Do { $response = read-hostwithdefault -default $Default_SyslogPort -prompt "Enter Syslog Port" } 
                while ((1..65535) -notcontains $response)
                
                $script:SysLogPort = $response
                Update-Status
            }
        },
        @{ 
            "Enabled" = $true;
            "ValidData" = $SysLogProtocol
            "Selectable" = $false
            "CompletedItemText" = $SysLogProtocol
            "Name" = "Set Syslog Protocol (Optional)"
            "Script" = { 

                Do { $response = read-hostwithdefault -default $Default_SyslogProtocol -prompt "Enter Syslog Protocol" } 
                while (("tcp","udp") -notcontains $response)
                
                $script:SysLogProtocol = $response
                Update-Status
            }
        },
        @{ 
            "Enabled" = $true;
            "ValidData" = $NtpServer
            "Selectable" = $false
            "CompletedItemText" = $NtpServer
            "Name" = "Set NTP Server"
            "Script" = { 

                #Get the response and ensure it is a valid IP
                [ipaddress]$TestIP = $null
                do { 
                    $response = read-host -prompt "Enter NTP Server ip address"
                } while ( -not ([ipaddress]::TryParse( $response, [ref]$TestIp)))

                $script:NtpServer = $response
                Update-Status
            }
        },
        @{ 
            "Enabled" = $true;
            "ValidData" = $DnsServer1
            "Selectable" = $false
            "CompletedItemText" = $DnsServer1
            "Name" = "Set Default Primary DNS Server";
            "Script" = { 

                #Get the response and ensure it is a valid IP
                [ipaddress]$TestIP = $null
                do { 
                    $response = read-host -prompt "Enter Primary DNS Server ip address"
                } while ( -not ([ipaddress]::TryParse( $response, [ref]$TestIp)))

                $script:DnsServer1 = $response
                Update-Status
            }
        },
        @{ 
            "Enabled" = $true;
            "ValidData" = $DnsServer2
            "Selectable" = $false
            "CompletedItemText" = $DnsServer2
            "Name" = "Set Default Secondary DNS Server";
            "Script" = { 

                #Get the response and ensure it is a valid IP
                [ipaddress]$TestIP = $null
                do { 
                    $response = read-host -prompt "Enter Secondary DNS Server ip address"
                } while ( -not ([ipaddress]::TryParse( $response, [ref]$TestIp)))

                $script:DnsServer2 = $response
                Update-Status
            }
        },
        @{ 
            "Enabled" = $true;
            "ValidData" = $DnsDomain
            "Selectable" = $false
            "CompletedItemText" = $DnsDomain
            "Name" = "Set Default DNS Domain Suffix";
            "Script" = { 

                #Get the response and ensure it is a valid IP
                do { 
                    [string]$response = read-host -prompt "Enter a valid DNS Domain Suffix"
                } while ( $response -notmatch $domain_regex )

                $script:DnsDomain = $response
                Update-Status
            }
        }
    )
}

$PocOptions = @{
    "Script" = { 
        $footer = {
            "***WARNING*** The options here are to enable rapid and reduced footprint deployments.  They are NOT SUPPORTED and must not be used in a production deployment ***WARNING***`nSingleControllerCluster: $SingleControllerCluster`nReduced Memory NSX Manager: $12GbManager"
            update-status
        }
        show-menu -menu $PocOptions | out-null 
    };
    "Enabled" = $true
    "Name" = "PoC Optimisation and unsupported options"
    "ValidData" = ( -not ( $SingleControllerCluster -or $12GbManager ))
    "Items" = @( 
        @{ 
            "Enabled" = $true
            "Selectable" = $true
            "Selected" = $SingleControllerCluster
            "Name" = "Deploy only one NSX controller"
            "Script" = { 

                if ( -not $SingleControllerCluster ) { 
                    $message  = "A single controller deployment is NOT SUPPORTED and should only be used for PoC's and lab environments"
                    $question = "Confirm?"
                    $decision = $Host.UI.PromptForChoice($message, $question, $yesnochoices, 1)

                    if ($decision -eq 0) {
                        $script:SingleControllerCluster = $true
                    }
                }
                else {
                    $script:SingleControllerCluster = $false
                }
                Update-Status
            }
        },
        @{ 
            "Enabled" = $true
            "Selectable" = $true
            "Selected" = $12GbManager
            "Name" = "Deploy NSX Manager with 12G Memory"
            "Script" = { 

                if ( -not $12GbManager ) { 
                    $message  = "A reduced NSX Manager memory footprint is NOT SUPPORTED and should only be used for PoC's and lab environments"
                    $question = "Confirm?"
                    $decision = $Host.UI.PromptForChoice($message, $question, $yesnochoices, 1)

                    if ($decision -eq 0) {
                        $script:12GbManager = $true
                    }
                }
                else {
                    $script:12GbManager = $false
                }
                Update-Status
            }
        }
    )
}


$Configuration = @{ 
    "Script" = { 
        $footer = {
            "Configuration Complete: $($Configuration.ValidData.tostring())"
            update-status
        }
        show-menu -menu $Configuration | out-null 
    };
    "Enabled" = $true;
    "Name" = "Configuration";
    "HelpText" = "Nothing"
    "ValidData" = $false;
    "Items" = @( 
        $vSphereConfiguration,
        $NetworkConfiguration,
        $NSXConfiguration,
        $PocOptions
    )
}

$rootmenu = @{ 
    "Name" = "Main Menu"
    "Items" = @( 
        $Configuration,
        @{
            "Enabled" = $false
            "Name" = "Installation"
            "HelpText" = "Complete all configuration items first."
            "ValidData" = $InstallationComplete
            "Script" = { 
                Invoke-Installation;
                $script:rootMenu.items[1].ValidData = $true
                "Installation Completed"
            }
        },
        @{
            "Enabled" = $true
            "Selectable" = $true
            "Name" = "Save Config"
            "HelpText" = "Save config to file."
            "Script" = { 
                save-config
                "Saved config"  
            }
        },
        @{
            "Enabled" = $true
            "Selectable" = $true
            "Name" = "Read Config"
            "HelpText" = "Read config from file."
            "Script" = { 
                read-config
            }
        }
        
    )
}

$Footer = { "Configuration Complete: $ConfigurationComplete" }

show-menu -menu $rootmenu






