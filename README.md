# README #

GuidedNsx.ps1 is a guided installation script for VMware NSX-v.

## Overview ##

GuidedNsx.ps1 is a menu driven simplified installation for VMware NSX-v that relies on PowerCLI and PowerNSX PowerShell modules to do the heavy lifting.  Ensure you are running the dev (soon to be v2) branch of PowerNSX before running this script.

The intent behind this script is to provide 'guide rails' and rapid deployment to allow PoC and simple production installations of NSX.

The current version is v1.1

## Prerequisites ##

1. A running vCenter server
2. One or more clusters to be prepared for NSX
3. Appropriately configured VDS and physical network (Single or multi VDS configuration is supported)
4. Available Management cluster/datastore/vd portgroup for Manager/Controller(s)

## Issues and Limitations ##

Because this is a simplified installation tool, not all valid configuration options are available (initially).  These limitations and any known issues are as follows:

1. Only SrcID based teaming for VTEP (no LACP/LAG/SrcMAC)
2. NSX Manager and NSX Controllers must be deployed to the same subnet and attached to the same portgroup.
3. Only DV Portgroups are supported for Manager and Controller.
4. No complexity validation is done on Manager/Controller password.  _Ensure_ you enter valid passwords.
5. Currently, no method to 'delete' an ip pool is available.  If you make a mistake entering an ip pools details, either create a new pool, or exit the tool and start again.  This will be resolved in a future update.
6. The Save Config option does not currently save passwords.  When loading a saved config, you must enter the vCenter connection password again, and re-enter the Manager/Controller passwords.
7. If installation fails for any reason, the script will exit, and cleanup will have to be manual.  Much care has been taken to guardrail all the inputs to make this much less likely, but this will continue to be improved in future versions as required.

## Installation ##
1) Install latest version of PowerNSX (currently dev branch is required.)

To get PowerNSX, run the following installation bootstrap in a powershell window (copy/paste, hit enter)

```
#!powershell

$url="https://bitbucket.org/nbradford/powernsx/raw/Dev/PowerNSXInstaller.ps1"; try { $wc = new-object Net.WebClient;$scr = try { $wc.DownloadString($url)} catch { if ( $_.exception.innerexception -match "(407)") { $wc.proxy.credentials = Get-Credential -Message "Proxy Authentication Required"; $wc.DownloadString($url) } else { throw $_ }}; $scr | iex } catch { throw $_ }

```

2) Download GuidedNsx.ps1 and save to a local directory

3) FROM A POWERCLI window, run GuidedNsx.ps1.

4) Use the menus to guide you through an installation of NSX.

## Contribution guidelines ##

Feel free to fork, commit and submit a pull request for any updates.  Feedback is welcome.  For feature requests, please use the 'Issues' page.

## Who do I talk to? ##

This tool is maintained by Nick Bradford (nbradford@vmware.com)